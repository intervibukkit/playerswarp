package ru.intervi.playerswarp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.io.File;
import java.io.IOException;
import java.util.UUID;
import java.util.Timer;
import java.util.TimerTask;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.Location;
import org.bukkit.Bukkit;

public class Config {
	public Config(Main main) {
		this.main = main;
		PATH = main.getDataFolder().getAbsoluteFile() + File.separator + "database.yml";
	}
	
	private Main main;
	private Timer timer = null;
	private final String PATH;
	
	public volatile ConcurrentHashMap<String, Location> warps = new ConcurrentHashMap<String, Location>();
	public volatile ConcurrentHashMap<UUID, ArrayList<String>> users = new ConcurrentHashMap<UUID, ArrayList<String>>();
	public volatile ConcurrentHashMap<String, Integer> costs = new ConcurrentHashMap<String, Integer>();
	
	public int cost = 0;
	public boolean chland = true;
	public int intsave = 300;
	public List<String> worlds = new ArrayList<String>();
	public boolean signs = true;
	
	public String reload = "";
	public String noperm = "";
	public String nomoney = ""; //%cost%
	public String create = ""; //%warp%
	public String busy = ""; //%warp%
	public String teleport = ""; //%warp%
	public String delete = ""; //%warp%
	public String notfound = ""; //%warp%
	public String listop = ""; //%page%, %pages%
	public String onlygame = "";
	public String error = "";
	public String clear = "";
	public String noowner = ""; //%warp%
	public String saved = "";
	public String deny = "";
	public String nosafe = ""; //%warp%
	public String withdraw = ""; //%cost%
	public String warpname = ""; //%warp%
	public String warpcost = ""; //%cost%
	public String warpowner = ""; //%name%
	public String warpcords = ""; //%cords%
	public String oksign = ""; //%warp%
	public String signcost = ""; //%cost%
	public String signowner = ""; //%name%
	public List<String> help = new ArrayList<String>();
	
	private static String color(String str) {
		return ChatColor.translateAlternateColorCodes('&', str);
	}
	
	private static List<String> color(List<String> list) {
		ArrayList<String> result = new ArrayList<String>();
		for (String s : list) result.add(color(s));
		return result;
	}
	
	public void load() {
		main.saveDefaultConfig();
		main.reloadConfig();
		FileConfiguration conf = main.getConfig();
		cost = conf.getInt("cost");
		chland = conf.getBoolean("chland");
		intsave = conf.getInt("intsave");
		worlds = conf.getStringList("worlds");
		signs = conf.getBoolean("signs");
		reload = color(conf.getString("reload"));
		noperm = color(conf.getString("noperm"));
		nomoney = color(conf.getString("nomoney"));
		create = color(conf.getString("create"));
		busy = color(conf.getString("busy"));
		teleport = color(conf.getString("teleport"));
		delete = color(conf.getString("delete"));
		notfound = color(conf.getString("notfound"));
		listop = color(conf.getString("listop"));
		onlygame = color(conf.getString("onlygame"));
		error = color(conf.getString("error"));
		clear = color(conf.getString("clear"));
		noowner = color(conf.getString("noowner"));
		saved = color(conf.getString("saved"));
		deny = color(conf.getString("deny"));
		nosafe = color(conf.getString("nosafe"));
		withdraw = color(conf.getString("withdraw"));
		warpname = color(conf.getString("warpname"));
		warpcost = color(conf.getString("warpcost"));
		warpowner = color(conf.getString("warpowner"));
		warpcords = color(conf.getString("warpcords"));
		oksign = color(conf.getString("oksign"));
		signcost = color(conf.getString("signcost"));
		signowner = color(conf.getString("signowner"));
		help = color(conf.getStringList("help"));
	}
	
	public void loadDB() {
		File file = new File(PATH);
		if (!file.isFile()) {
			try {file.createNewFile();}
			catch(IOException e) {e.printStackTrace();}
		}
		FileConfiguration db = YamlConfiguration.loadConfiguration(file);
		if (!db.isSet("warps") || !db.isSet("users")) return;
		for (String s : db.getStringList("warps")) {
			String name = s.substring(0, s.indexOf('|')).trim().toLowerCase();
			String coords = s.substring(s.indexOf('|')+1);
			String world = coords.substring(0, coords.indexOf(';'));
			String xyz[] = coords.substring(coords.indexOf(';')+1).split(";");
			double x = Double.parseDouble(xyz[0]),
					y = Double.parseDouble(xyz[1]),
					z = Double.parseDouble(xyz[2]);
			float yaw = Float.parseFloat(xyz[3]),
					pitch = Float.parseFloat(xyz[4]);
			Location loc = new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch);
			if (warps.containsKey(name)) warps.replace(name, loc);
			else warps.put(name, loc);
		}
		for (String s : db.getStringList("users")) {
			UUID uuid = UUID.fromString(s.substring(0, s.indexOf('|')).trim());
			String names[] = s.substring(s.indexOf('|')+1).trim().split(";");
			ArrayList<String> value = new ArrayList<String>();
			value.addAll(Arrays.asList(names));
			if (users.containsKey(uuid)) users.replace(uuid, value);
			else users.put(uuid, value);
		}
		if (!db.isSet("costs")) return;
		for (String s : db.getStringList("costs")) {
			String name = s.substring(0, s.indexOf('|')).trim().toLowerCase();
			Integer value = Integer.decode(s.substring(s.indexOf('|')+1).trim());
			if (costs.containsKey(name)) costs.replace(name, value);
			else costs.put(name, value);
		}
	}
	
	public void save() {
		YamlConfiguration db = new YamlConfiguration();
		if (!warps.isEmpty()) {
			ArrayList<String> w = new ArrayList<String>();
			for (Entry<String, Location> entry : warps.entrySet()) {
				Location loc = entry.getValue();
				w.add(
						entry.getKey() + '|' + loc.getWorld().getName() + ';' + loc.getX() + ';' + loc.getY() + ';' +
						loc.getZ() + ';' + loc.getYaw() + ';' + loc.getPitch()
						);
			}
			db.set("warps", w);
		}
		if (!users.isEmpty()) {
			ArrayList<String> u = new ArrayList<String>();
			for (Entry<UUID, ArrayList<String>> entry : users.entrySet()) {
				String names = "";
				for (String s : entry.getValue()) names += s + ';';
				if (!names.isEmpty()) names = names.substring(0, names.length()-1);
				u.add(entry.getKey().toString() + '|' + names);
			}
			db.set("users", u);
		}
		if (!costs.isEmpty()) {
			ArrayList<String> c = new ArrayList<String>();
			for (Entry<String, Integer> entry : costs.entrySet()) c.add(entry.getKey() + '|' + entry.getValue().toString());
			db.set("costs", c);
		}
		try {db.save(PATH);}
		catch(IOException e) {e.printStackTrace();}
	}
	
	private class Saver extends TimerTask {
		@Override
		public void run() {
			save();
		}
	}
	
	public void startTimer() {
		if (intsave <= 0) return;
		timer = new Timer();
		timer.schedule(new Saver(), intsave*1000, intsave*1000);
	}
	
	public void stopTimer() {
		if (timer != null) {
			timer.cancel();
			timer = null;
		}
	}
	
	public void clear() {
		warps.clear();
		users.clear();
		costs.clear();
	}
}
