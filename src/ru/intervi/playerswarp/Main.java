package ru.intervi.playerswarp;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.command.CommandSender;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.block.Sign;
import org.bukkit.Material;

import net.milkbowl.vault.Vault;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import net.milkbowl.vault.economy.EconomyResponse.ResponseType;

import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.UUID;

public class Main extends JavaPlugin implements Listener {
	private Config conf = new Config(this);
	private Economy eco = null;
	
	@Override
	public void onEnable() {
		if(Bukkit.getPluginManager().getPlugin("Vault") instanceof Vault) {
        	RegisteredServiceProvider<Economy> service = Bukkit.getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
            if(service != null) eco = service.getProvider();
            else {
            	getLogger().warning("Vault not found");
            	return;
            }
		}
		conf.load();
		conf.loadDB();
		conf.startTimer();
		getServer().getPluginManager().registerEvents(this, this);
	}
	
	@Override
	public void onDisable() {
		conf.stopTimer();
		conf.save();
		conf.clear();
	}
	
	@EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
	public void onChange(SignChangeEvent event) {
		if (!conf.signs) return;
		Player player = event.getPlayer();
		if (player.hasPermission("playerswarp.signcreate")) {
			String lines[] = event.getLines();
			if (lines == null || lines.length == 2) return;
			if (!lines[0].toLowerCase().equals("[pwarp]")) return;
			String warp = lines[1].toLowerCase();
			if (!conf.warps.containsKey(warp)) {
				player.sendMessage(conf.notfound.replaceAll("%warp%", lines[1]));
				return;
			}
			event.setLine(1, warp);
			if (conf.costs.containsKey(warp)) event.setLine(2, conf.signcost.replaceAll("%cost%", conf.costs.get(warp).toString()));
			String owner = "";
			OfflinePlayer powner = Bukkit.getOfflinePlayer(getOwner(warp));
			if (powner != null) owner = powner.getName();
			event.setLine(3, conf.signowner.replaceAll("%name%", owner));
			player.sendMessage(conf.oksign.replaceAll("%warp%", lines[1]));
		} else player.sendMessage(conf.noperm);
	}
	
	@EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
	public void onInteract(PlayerInteractEvent event) {
		if (!conf.signs) return;
		if (!event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) return;
		Material type = event.getClickedBlock().getType();
		if (!type.equals(Material.WALL_SIGN) && !type.equals(Material.SIGN_POST)) return;
		Sign sign = (Sign) event.getClickedBlock().getState();
		String lines[] = sign.getLines();
		if (!lines[0].toLowerCase().equals("[pwarp]")) return;
		Player player = event.getPlayer();
		if (player.hasPermission("playerswarp.signuse")) {
			String warp = lines[1].toLowerCase();
			teleport(player, warp);
		} else player.sendMessage(conf.noperm);
	}
	
	private String[] getList(int page) {
		ArrayList<String> result = new ArrayList<String>();
		String warps[] = conf.warps.keySet().toArray(new String[conf.warps.size()]);
		result.add(conf.listop.replaceAll("%page%", String.valueOf(page)).replaceAll("%pages%", String.valueOf((int) Math.ceil((warps.length/10)))));
		if (conf.warps.isEmpty()) return result.toArray(new String[result.size()]);
		int to = page*10-1;
		if (to >= warps.length) to = warps.length-1;
		int from = to-9;
		if (from < 0) from = 0;
		for (int i = from; i <= to; i++)
			result.add(conf.costs.containsKey(warps[i]) ? warps[i] + '(' + conf.costs.get(warps[i].toString()) + ')' : warps[i]);
		return result.toArray(new String[result.size()]);
	}
	
	private UUID getOwner(String warp) {
		for (Entry<UUID, ArrayList<String>> entry : conf.users.entrySet()) {
			if (entry.getValue().contains(warp)) return entry.getKey();
		}
		return null;
	}
	
	private void teleport(Player player, String warp) {
		if (!conf.warps.containsKey(warp)) {
			player.sendMessage(conf.notfound.replaceAll("%warp%", warp));
			return;
		}
		double cost = 0;
		if (conf.costs.containsKey(warp)) cost = conf.costs.get(warp).doubleValue();
		if (eco != null && cost > 0 && !player.hasPermission("playerswarp.nocost")) {
			if (eco.has(player, cost)) {
				OfflinePlayer owner = null;
				for (Entry<UUID, ArrayList<String>> entry : conf.users.entrySet()) {
					if (entry.getValue().contains(warp)) {
						owner = Bukkit.getOfflinePlayer(entry.getKey());
						break;
					}
				}
				if (owner != null && !player.getUniqueId().equals(owner.getUniqueId())) {
					EconomyResponse er = eco.withdrawPlayer(player, cost);
					ResponseType type = er.type;
					if (type.equals(ResponseType.SUCCESS)) {
						if (owner != null) eco.depositPlayer(owner, cost);
					} else {
						getLogger().warning("Vault error withdraw from " + player.getName() + ' ' + type.toString() + ": " + er.errorMessage);
						player.sendMessage(conf.error);
						return;
					}
				} else cost = 0;
			} else {
				player.sendMessage(conf.nomoney.replaceAll("%cost%", String.valueOf(cost)));
				return;
			}
		} else cost = 0;
		Location loc = conf.warps.get(warp);
		if (loc == null || loc.getWorld() == null) {
			player.sendMessage(conf.error);
			return;
		}
		if (conf.chland && !player.hasPermission("playerswarp.nosafe")) {
			Block block = loc.getBlock();
			Block blocks[] = {
					block.getRelative(0, 1, 0), block.getRelative(0, -1, 0),
					block.getRelative(0, -2, 0), block.getRelative(0, -3, 0)
			};
			if (!block.isEmpty() || !blocks[0].isEmpty() ||
					((blocks[1].isEmpty() || blocks[1].isLiquid()) && (blocks[2].isEmpty() || blocks[2].isLiquid()) &&
							(blocks[3].isEmpty() || blocks[3].isLiquid()))) {
				player.sendMessage(conf.nosafe.replaceAll("%warp%", warp));
				return;
			}
		}
		player.teleport(loc);
		player.sendMessage(conf.teleport.replaceAll("%warp%", warp));
		if (eco != null && cost > 0) player.sendMessage(conf.withdraw.replaceAll("%cost%", String.valueOf(cost)));
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		boolean isplayer = false;
		if (sender instanceof Player) isplayer = true;
		switch(command.getLabel().toLowerCase()) {
		case "pwreload":
			if (sender.hasPermission("playerswarp.reload")) {
				conf.load();
				sender.sendMessage(conf.reload);
			} else sender.sendMessage(conf.noperm);
			break;
		case "pwreload-db":
			if (sender.hasPermission("playerswarp.reload")) {
				conf.loadDB();
				sender.sendMessage(conf.reload);
			} else sender.sendMessage(conf.noperm);
			break;
		case "pwsave":
			if (sender.hasPermission("playerswarp.save")) {
				conf.save();
				sender.sendMessage(conf.saved);
			} else sender.sendMessage(conf.noperm);
			break;
		case "pwclear":
			if (sender.hasPermission("playerswarp.clear")) {
				conf.clear();
				sender.sendMessage(conf.clear);
			} else sender.sendMessage(conf.noperm);
			break;
		case "psetwarp":
			if (sender.hasPermission("playerswarp.setwarp")) {
				if (!isplayer) {
					sender.sendMessage(conf.onlygame);
					break;
				}
				if (args.length < 1 || (args.length >= 2 && !args[1].matches("^[0-9]*$"))) {
					for (String s : conf.help) sender.sendMessage(s);
					break;
				}
				String warp = args[0].toLowerCase();
				UUID uuid = ((Player) sender).getUniqueId();
				if (conf.warps.containsKey(warp) && !uuid.equals(getOwner(warp))) {
					sender.sendMessage(conf.busy.replaceAll("%warp%", args[0]));
					break;
				}
				Location loc = ((Player) sender).getLocation();
				if (conf.worlds.contains(loc.getWorld().getName().toLowerCase())) {
					sender.sendMessage(conf.deny);
					break;
				}
				if (eco != null) {
					if (args.length >= 2) conf.costs.put(warp, Integer.decode(args[1]));
					if (conf.cost > 0 && !sender.hasPermission("playerswarp.nocost") && !conf.warps.containsKey(warp)) {
						if (!eco.has((Player) sender, conf.cost)) {
							sender.sendMessage(conf.nomoney.replaceAll("%cost%", String.valueOf(conf.cost)));
							break;
						}
						ResponseType type = eco.withdrawPlayer(((Player) sender), conf.cost).type;
						if (!type.equals(ResponseType.SUCCESS)) {
							getLogger().warning("Vault error withdraw from " + ((Player) sender).getName());
							sender.sendMessage(conf.error);
							break;
						}
					}
				}
				if (conf.warps.containsKey(warp)) conf.warps.replace(warp, loc);
				else conf.warps.put(warp, loc);
				ArrayList<String> list = null;
				if (conf.users.containsKey(uuid)) list = conf.users.get(uuid);
				else list = new ArrayList<String>();
				list.add(warp);
				if (conf.users.containsKey(uuid)) conf.users.replace(uuid, list);
				else conf.users.put(uuid, list);
				sender.sendMessage(conf.create.replaceAll("%warp%", args[0]));
			} else sender.sendMessage(conf.noperm);
			break;
		case "pdelwarp":
			if (sender.hasPermission("playerswarp.delwarp")) {
				if (args.length < 1) {
					for (String s : conf.help) sender.sendMessage(s);
					break;
				}
				String warp = args[0].toLowerCase();
				if (!conf.warps.containsKey(warp)) {
					sender.sendMessage(conf.notfound.replaceAll("%warp%", args[0]));
					break;
				}
				if (isplayer && !sender.hasPermission("playerswarp.delall") && !((Player) sender).getUniqueId().equals(getOwner(warp))) {
					sender.sendMessage(conf.noowner.replaceAll("%warp%", args[0]));
					break;
				}
				conf.warps.remove(warp);
				conf.costs.remove(warp);
				if (!isplayer || sender.hasPermission("playerswarp.delall")) {
					for (Entry<UUID, ArrayList<String>> entry : conf.users.entrySet()) {
						ArrayList<String> list = entry.getValue();
						if (list.contains(warp)) {
							list.remove(warp);
							conf.users.replace(entry.getKey(), list);
							break;
						}
					}
				} else {
					ArrayList<String> list = conf.users.get(((Player) sender).getUniqueId());
					list.remove(warp);
					conf.users.replace(((Player) sender).getUniqueId(), list);
				}
				sender.sendMessage(conf.delete.replaceAll("%warp%", args[0]));
			} else sender.sendMessage(conf.noperm);
			break;
		case "pwarp":
			if (sender.hasPermission("playerswarp.warp")) {
				if (!isplayer) {
					sender.sendMessage(conf.onlygame);
					break;
				}
				if (args.length < 1) {
					if (sender.hasPermission("playerswarp.list")) sender.sendMessage(getList(1));
					else for (String s : conf.help) sender.sendMessage(s);
					break;
				}
				teleport(((Player) sender), args[0].toLowerCase());
			} else sender.sendMessage(conf.noperm);
			break;
		case "pwarplist":
			if (sender.hasPermission("playerswarp.list")) {
				int page = 1;
				if (args.length >= 1 && args[0].matches("^[0-9]*$")) page = Integer.parseInt(args[0]);
				if (page <= 0) page = 1;
				sender.sendMessage(getList(page));
			} else sender.sendMessage(conf.noperm);
			break;
		case "pwarpinfo":
			if (sender.hasPermission("playerswarp.info")) {
				if (args.length < 1) {
					for (String s : conf.help) sender.sendMessage(s);
					break;
				}
				String warp = args[0].toLowerCase();
				if (!conf.warps.containsKey(warp)) {
					sender.sendMessage(conf.notfound.replaceAll("%warp%", args[0]));
					break;
				}
				int cost = 0;
				String owner = "", cords = "";
				OfflinePlayer powner = Bukkit.getOfflinePlayer(getOwner(warp));
				if (powner != null) owner = powner.getName();
				Location loc = conf.warps.get(warp);
				if (loc != null && loc.getWorld() != null)
					cords = loc.getWorld().getName() + " - " + loc.getBlockX() + ' ' + loc.getBlockY() + ' ' + loc.getBlockZ();
				if (conf.costs.containsKey(warp)) cost = conf.costs.get(warp).intValue();
				sender.sendMessage(conf.warpname.replaceAll("%warp%", warp));
				sender.sendMessage(conf.warpowner.replaceAll("%name%", owner));
				sender.sendMessage(conf.warpcost.replaceAll("%cost%", String.valueOf(cost)));
				sender.sendMessage(conf.warpcords.replaceAll("%cords%", cords));
			} else sender.sendMessage(conf.noperm);
			break;
		case "pwhelp":
			for (String s: conf.help) sender.sendMessage(s);
			break;
		default:
			return false;
		}
		return true;
	}
}
